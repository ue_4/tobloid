// Tobloid by Emil Whind

#pragma once

#include "CoreMinimal.h"
#include "Pickups/TBLBasePickup.h"
#include "TBLPickupableCube.generated.h"

/**
 *
 */
UCLASS()
class TOBLOID_API ATBLPickupableCube : public ATBLBasePickup
{
    GENERATED_BODY()

protected:
    virtual void Launch() override;
};
