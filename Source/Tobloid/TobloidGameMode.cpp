// Copyright Epic Games, Inc. All Rights Reserved.

#include "TobloidGameMode.h"
#include "TobloidCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATobloidGameMode::ATobloidGameMode()
{
    // set default pawn class to our Blueprinted character
    static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
    if (PlayerPawnBPClass.Class != NULL)
    {
        DefaultPawnClass = ATobloidCharacter::StaticClass();
    }
}
