// Tobloid by Emil Whind

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TBLPickupComponent.generated.h"

class ATobloidCharacter;
class ATBLBasePickup;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TOBLOID_API UTBLPickupComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UTBLPickupComponent();

    void PickupItem(ATBLBasePickup* Item);

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
    class UInputAction* ThrowAction;

    UFUNCTION(BlueprintCallable, Category = "Pickup")
    void Throw();

    UFUNCTION(BlueprintCallable, Category = "Pickup")
    int GetOwnedItemsNum() { return OwnedItems.Num(); }

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pickup")
    TArray<ATBLBasePickup*> OwnedItems;


private:
};
