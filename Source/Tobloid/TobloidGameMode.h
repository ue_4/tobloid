// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TobloidGameMode.generated.h"

UCLASS(minimalapi)
class ATobloidGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATobloidGameMode();
};



