// Tobloid by Emil Whind

#include "Components/TBLPickupComponent.h"
// #include "GameFramework/Character.h"
#include "Pickups/TBLBasePickup.h"
#include "../../TobloidCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogTBLPickupComponent, All, All);

// Sets default values for this component's properties
UTBLPickupComponent::UTBLPickupComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UTBLPickupComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UTBLPickupComponent::PickupItem(ATBLBasePickup* Item)
{
    if (!Item) return;

    if (OwnedItems.Num() > 0 && Item == OwnedItems.Last()) return;

    if (OwnedItems.Num() < 3)
    {
        ATobloidCharacter* OwnerCharacter = Cast<ATobloidCharacter>(GetOwner());
        if (!OwnerCharacter) return;

        Item->AttachToOwner(OwnerCharacter);
        OwnedItems.Add(Item);
    }
}

void UTBLPickupComponent::Throw()
{
    if (OwnedItems.Num() < 1) return;

    OwnedItems.Last()->DeattachFromOwner(EDeattachType::Thrown);
    OwnedItems.RemoveAt(OwnedItems.Num() - 1);
}