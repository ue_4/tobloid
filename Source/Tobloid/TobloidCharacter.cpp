// Copyright Epic Games, Inc. All Rights Reserved.

#include "TobloidCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/TBLPickupComponent.h"
#include "Components/TBLCharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "Components/TBLEnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Public/Pickups/TBLBasePickup.h"

DEFINE_LOG_CATEGORY_STATIC(LogTobloidCharacter, All, All);


ATobloidCharacter::ATobloidCharacter(const FObjectInitializer& ObjInit)
    : Super(ObjInit.SetDefaultSubobjectClass<UTBLCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
    TBLCharacterMovement = Cast<UTBLCharacterMovementComponent>(GetMovementComponent());
    if (!TBLCharacterMovement) return;

    TBLCharacterMovement->DefaultParams();
    EnhancedInputComponent = CreateDefaultSubobject<UTBLEnhancedInputComponent>(TEXT("EnhancedInputComponent"));
    PickupComponent = CreateDefaultSubobject<UTBLPickupComponent>(TEXT("PickupComponent"));

    // Set size for collision capsule
    GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

    // Don't rotate when the controller rotates. Let that just affect the camera.
    bUseControllerRotationPitch = false;
    bUseControllerRotationYaw = false;
    bUseControllerRotationRoll = false;

    // Create a camera boom (pulls in towards the player if there is a collision)
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->TargetArmLength = 400.0f;       // The camera follows at this distance behind the character
    CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

    // Create a follow camera
    FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
    FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom
                                                                                // adjust to match the controller orientation
    FollowCamera->bUsePawnControlRotation = false;                              // Camera does not rotate relative to arm

    // Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character)
    // are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)
}

void ATobloidCharacter::BeginPlay()
{
    // Call the base class
    Super::BeginPlay();

    // Add Input Mapping Context
    if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
    {
        if (UEnhancedInputLocalPlayerSubsystem* Subsystem =
                ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
        {
            Subsystem->AddMappingContext(DefaultMappingContext, 0);
        }
    }
}

void ATobloidCharacter::NotifyActorBeginOverlap(AActor* OtherActor)
{
    Super::NotifyActorBeginOverlap(OtherActor);

    if (ATBLBasePickup* ActorHit = CastChecked<ATBLBasePickup>(OtherActor))
    {
        PickupComponent->PickupItem(ActorHit);
    }
}

int ATobloidCharacter::GetOwnedItemsNum()
{
    return PickupComponent->GetOwnedItemsNum();
}

void ATobloidCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
    EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
    EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

    EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, TBLCharacterMovement, &UTBLCharacterMovementComponent::Move);

    EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, TBLCharacterMovement, &UTBLCharacterMovementComponent::Look);

    EnhancedInputComponent->BindAction(ThrowAction, ETriggerEvent::Triggered, PickupComponent, &UTBLPickupComponent::Throw);
}
