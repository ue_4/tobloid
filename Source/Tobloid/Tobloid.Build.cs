// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Tobloid : ModuleRules
{
    public Tobloid(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "EnhancedInput" });

        PublicIncludePaths.AddRange(new string[] { "Tobloid/Public/Pickups", "Tobloid/Public/Components" });
    }
}
