// Tobloid by Emil Whind

#include "Pickups/TBLBasePickup.h"
// #include "Components/BoxComponent.h"
#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(LogBasePickup, All, All);

// Sets default values
ATBLBasePickup::ATBLBasePickup()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = false;

    AddSceneComponent();
}

void ATBLBasePickup::NotifyActorBeginOverlap(AActor* OtherActor)
{
    Super::NotifyActorBeginOverlap(OtherActor);
}

void ATBLBasePickup::BeginPlay()
{
    Super::BeginPlay();
}

void ATBLBasePickup::AddSceneComponent()
{
    SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
    if (!SceneComponent) return;
    SetRootComponent(SceneComponent);

    ItemMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
    if (!ItemMeshComponent) return;

    ItemMeshComponent->SetupAttachment(SceneComponent);
    ItemMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    ItemMeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

void ATBLBasePickup::AttachToOwner(ATobloidCharacter* NewOnwer)
{
    if (!NewOnwer || !ItemMeshComponent) return;

    SetOwner(NewOnwer);
    ItemMeshComponent->SetSimulatePhysics(false);
    ItemMeshComponent->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
    ItemMeshComponent->AttachToComponent(SceneComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
    FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
    AttachToComponent(NewOnwer->GetMesh(), AttachmentRules, PickupSocket);
}

void ATBLBasePickup::DeattachFromOwner(EDeattachType DeattachType)
{
    ItemMeshComponent->SetSimulatePhysics(true);
    ItemMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    FDetachmentTransformRules DetachmentRules(EDetachmentRule::KeepWorld, false);
    DetachFromActor(DetachmentRules);

    switch (DeattachType)
    {
    case EDeattachType::Thrown:
    {
        UE_LOG(LogBasePickup, Display, TEXT("THE ITEM HAS BEEN THROWN!"));
        Launch();
    }
    break;

    case EDeattachType::OwnerDead:
        break;

    default:
        break;
    }

    SetOwner(nullptr);
}

APlayerController* ATBLBasePickup::GetPlayerController() const
{
    const ACharacter* Player = Cast<ACharacter>(GetOwner());
    if (!Player) return nullptr;

    return Player->GetController<APlayerController>();
}

bool ATBLBasePickup::GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const
{
    const APlayerController* Controller = GetPlayerController();
    if (!Controller) return false;

    Controller->GetPlayerViewPoint(ViewLocation, ViewRotation); // to be set by links

    return true;
}

bool ATBLBasePickup::GetTraceData(FVector& CameraLocation, FVector& StartLocation, FVector& TraceEndLocation) const
{
    FVector ViewLocation;
    FRotator ViewRotation;
    if (!GetPlayerViewPoint(ViewLocation, ViewRotation)) return false; // to be set by links

    CameraLocation = ViewLocation;
    const FVector TraceViewDirection = ViewRotation.Vector();
    StartLocation = GetItemLocation() + TraceViewDirection * ThrowingOffset;
    TraceEndLocation = CameraLocation + TraceViewDirection * TraceMaxDistance;
    return true;
}

ATobloidCharacter* ATBLBasePickup::GetTobloidOwner() const
{
    return Cast<ATobloidCharacter>(GetOwner());
}

void ATBLBasePickup::Launch()
{
    if (!GetWorld()) return;

    FVector CameraLocation, StartLocation, TraceEndLocation;
    if (!GetTraceData(CameraLocation, StartLocation, TraceEndLocation)) return;

    FHitResult HitResult;
    HitTrace(HitResult, CameraLocation, StartLocation, TraceEndLocation);

    if (HitResult.bBlockingHit)
    {
        // BringDamage(HitResult);
        UE_LOG(LogTemp, Display, TEXT("Bone: %s"), *HitResult.BoneName.ToString());
        DrawDebugSphere(GetWorld(), TraceEndLocation, 10.0f, 24, FColor::Red, false, 5.0f);
    }

    DrawDebugLine(GetWorld(), StartLocation, TraceEndLocation, FColor::Red, false, 3.0f, 0, 3.0f);
}

void ATBLBasePickup::HitTrace(FHitResult& HitResult, const FVector& CameraLocation, const FVector& StartLocation, FVector& TraceEndLocation)
{
    FCollisionQueryParams CollisionParams;
    CollisionParams.AddIgnoredActor(GetOwner());

    AimTrace(HitResult, CameraLocation, TraceEndLocation, CollisionParams);

    if (HitResult.bBlockingHit)
    {
        TraceEndLocation = HitResult.ImpactPoint;
    }

    FHitResult BulletlHitResult;
    BulletTrace(BulletlHitResult, StartLocation, TraceEndLocation, CollisionParams);

    if (BulletlHitResult.bBlockingHit)
    {
        HitResult = BulletlHitResult;
        TraceEndLocation = HitResult.ImpactPoint;
    }
}

void ATBLBasePickup::AimTrace(
    FHitResult& AimHitResult, const FVector& CameraLocation, FVector& TraceEndLocation, const FCollisionQueryParams& CollisionParams)
{
    GetWorld()->LineTraceSingleByChannel(
        AimHitResult, CameraLocation, TraceEndLocation, ECollisionChannel::ECC_Visibility, CollisionParams);
}

void ATBLBasePickup::BulletTrace(
    FHitResult& BulletlHitResult, const FVector& StartLocation, FVector& TraceEndLocation, const FCollisionQueryParams& CollisionParams)
{
    GetWorld()->LineTraceSingleByChannel(
        BulletlHitResult, StartLocation, TraceEndLocation, ECollisionChannel::ECC_Visibility, CollisionParams);
}

FVector ATBLBasePickup::GetItemLocation() const
{
    ATobloidCharacter* CurrentOwner = GetTobloidOwner();
    if (CurrentOwner)
    {
        return CurrentOwner->GetMesh()->GetSocketLocation(PickupSocket);
    }
    else
    {
        return GetActorLocation();
    }
}
