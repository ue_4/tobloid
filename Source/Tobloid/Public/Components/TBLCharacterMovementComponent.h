// Tobloid by Emil Whind

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TBLCharacterMovementComponent.generated.h"

struct FInputActionValue;

/**
 *
 */
UCLASS()
class TOBLOID_API UTBLCharacterMovementComponent : public UCharacterMovementComponent
{
    GENERATED_BODY()

    public:

    virtual void BeginPlay() override;

    void DefaultParams();

    void Move(const FInputActionValue& Value);

    void Look(const FInputActionValue& Value);
};
