// Tobloid by Emil Whind

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../TobloidCharacter.h"
#include "TBLBasePickup.generated.h"

// class UBoxComponent;

UENUM(BlueprintType)
enum class EDeattachType : uint8
{
    OwnerDead,
    Thrown
};

UCLASS()
class TOBLOID_API ATBLBasePickup : public AActor
{
    GENERATED_BODY()

public:
    ATBLBasePickup();

protected:
    virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

    virtual void BeginPlay() override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    USceneComponent* SceneComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UStaticMeshComponent* ItemMeshComponent;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FName PickupSocket = "PickupSocket";

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Throwing")
    FVector ThrowingOffset = FVector(100, 100, 100);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Throwing")
    FVector ThrowingStrenght = FVector(100, 100, 100);

    UFUNCTION(BlueprintCallable, Category = "Pickup")
    APlayerController* GetPlayerController() const;

    UFUNCTION(BlueprintCallable, Category = "Pickup")
    ATobloidCharacter* GetTobloidOwner() const;

    UFUNCTION(BlueprintCallable, Category = "Pickup")
    void AddSceneComponent();

    bool GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const;
    virtual bool GetTraceData(FVector& CameraLocation, FVector& StartLocation, FVector& TraceEndLocation) const;
    float TraceMaxDistance = 1500.0f;
    virtual void Launch();
    void HitTrace(FHitResult& HitResult, const FVector& CameraLocation, const FVector& StartLocation, FVector& TraceEndLocation);
    void AimTrace(
        FHitResult& AimHitResult, const FVector& CameraLocation, FVector& TraceEndLocation, const FCollisionQueryParams& CollisionParams);
    void BulletTrace(FHitResult& BulletlHitResult, const FVector& MuzzleLocation, FVector& TraceEndLocation,
        const FCollisionQueryParams& CollisionParams);
    FVector GetItemLocation() const;

public:
    UFUNCTION(BlueprintCallable, Category = "Pickup")
    void DeattachFromOwner(EDeattachType DeattachType);

    UFUNCTION(BlueprintCallable, Category = "Pickup")
    void AttachToOwner(ATobloidCharacter* NewOwner);
};
