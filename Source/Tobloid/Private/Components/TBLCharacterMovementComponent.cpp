// Tobloid by Emil Whind

#include "Components/TBLCharacterMovementComponent.h"
#include "EnhancedInputSubsystems.h"

void UTBLCharacterMovementComponent::BeginPlay()
{
    Super::BeginPlay();

    // DefaultParams();
}

void UTBLCharacterMovementComponent::DefaultParams()
{
    bOrientRotationToMovement = true;            // Character moves in the direction of input...
    RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

    JumpZVelocity = 701.f;
    AirControl = 0.35f;
    MaxWalkSpeed = 500.f;
    MinAnalogWalkSpeed = 20.f;
    BrakingDecelerationWalking = 2000.f;
}

void UTBLCharacterMovementComponent::Move(const FInputActionValue& Value)
{
    // input is a Vector2D
    FVector2D MovementVector = Value.Get<FVector2D>();

    APawn* Owner = CastChecked<APawn>(GetOwner());
    if (!Owner) return;

    AController* Controller = Owner->GetController();
    if (!Controller) return;

    // find out which way is forward
    const FRotator Rotation = Controller->GetControlRotation();
    const FRotator YawRotation(0, Rotation.Yaw, 0);

    // get forward vector
    const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

    // get right vector
    const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

    // add movement
    Owner->AddMovementInput(ForwardDirection, MovementVector.Y);
    Owner->AddMovementInput(RightDirection, MovementVector.X);
}

void UTBLCharacterMovementComponent::Look(const FInputActionValue& Value)
{

    // input is a Vector2D
    FVector2D LookAxisVector = Value.Get<FVector2D>();

    APawn* Owner = CastChecked<APawn>(GetOwner());
    if (!Owner) return;

    AController* Controller = Owner->GetController();
    if (!Controller) return;

    // add yaw and pitch input to controller
    Owner->AddControllerYawInput(LookAxisVector.X);
    Owner->AddControllerPitchInput(LookAxisVector.Y);
}
