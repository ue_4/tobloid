// Tobloid by Emil Whind

#include "Pickups/TBLPickupableCube.h"

void ATBLPickupableCube::Launch()
{
    if (!GetWorld()) return;

    FVector CameraLocation, StartLocation, TraceEndLocation;
    if (!GetTraceData(CameraLocation, StartLocation, TraceEndLocation)) return;

    FHitResult HitResult;
    HitTrace(HitResult, CameraLocation, StartLocation, TraceEndLocation);

    FVector Direction = (TraceEndLocation - StartLocation).GetSafeNormal();
    const FTransform SpawnTransform(FRotator::ZeroRotator, StartLocation);

    
    FVector ViewLocation;
    FRotator ViewRotation;
    if (!GetPlayerViewPoint(ViewLocation, ViewRotation)) return;

    SetActorLocation(StartLocation);
    ItemMeshComponent->AddImpulse(ViewRotation.Vector() * ThrowingStrenght);

    if (HitResult.bBlockingHit)
    {
        // BringDamage(HitResult);
        UE_LOG(LogTemp, Display, TEXT("Bone: %s"), *HitResult.BoneName.ToString());
        DrawDebugSphere(GetWorld(), TraceEndLocation, 10.0f, 24, FColor::Red, false, 5.0f);
    }

    DrawDebugLine(GetWorld(), StartLocation, TraceEndLocation, FColor::Red, false, 3.0f, 0, 3.0f);
}
