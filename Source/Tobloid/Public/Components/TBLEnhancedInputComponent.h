// Tobloid by Emil Whind

#pragma once

#include "CoreMinimal.h"
#include "EnhancedInputComponent.h"
#include "TBLEnhancedInputComponent.generated.h"

/**
 *
 */
UCLASS()
class TOBLOID_API UTBLEnhancedInputComponent : public UEnhancedInputComponent
{
    GENERATED_BODY()
};
